<?php
/*
 * This file is part of aspetos.
 *
 * (c)2015 Ludwig Ruderstaller <lr@cwd.at>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace Cwd\FroalaBundle\Twig;

use Zitate\Model\Entity\Newsletter;
use Zitate\Service\NewsletterService;

/**
 * Class FroalaExtension
 *
 * @package Cwd\FroalaBundle\Twig
 * @author  Ludwig Ruderstaller <lr@cwd.at>
 */
class FroalaExtension extends \Twig_Extension
{
    /**
     * @var array
     */
    protected $config;

    /**
     * @param array $config
     */
    public function __construct($config)
    {
        $this->config = $config;
    }

    /**
     * @return \Twig_SimpleFunction[]
     */
    public function getFunctions()
    {
        return [
            new \Twig_SimpleFunction(
                'cwd_froala_javascript',
                [$this, 'getJavascript'],
                [
                    'is_safe'           => ['html'],
                    'needs_environment' => true,
                ]
            ),
        ];
    }

    /**
     * @param \Twig_Environment $twig
     * @param array             $runtimeOptions
     *
     * @return string
     */
    public function getJavascript(\Twig_Environment $twig, $runtimeOptions = [])
    {
        $config = array_merge(
            $this->config['default_js_options'],
            $this->config['js_options'],
            $runtimeOptions
        );

        return $twig->render('CwdFroalaBundle::script.html.twig', ['config' => $config]);
    }

    /**
    /**
     *
     * @return string
     */
    public function getName()
    {
        return 'cwd_froala.twig.javascript';
    }
}
