<?php

namespace Cwd\FroalaBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Class CwdFroalaBundle
 * @package Zitate\Bundle\FrontendBundle
 */
class CwdFroalaBundle extends Bundle
{
}
