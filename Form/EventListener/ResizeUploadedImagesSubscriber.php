<?php
/*
 * This file is part of CwdFroalaBundle
 *
 * (c)2016 Ludwig Ruderstaller <lr@cwd.at>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

namespace Cwd\FroalaBundle\Form\EventListener;

use Cwd\MediaBundle\Service\MediaService;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;

/**
 * Class ResizeUploadedImagesSubscriber
 * @package Cwd\FroalaBundle\Form\EventListener
 */
class ResizeUploadedImagesSubscriber implements EventSubscriberInterface
{

    /**
     * @var MediaService
     */
    protected $mediaService;

    /**
     * ResizeUploadedImagesSubscriber constructor.
     * @param MediaService $mediaService
     */
    public function __construct(MediaService $mediaService = null)
    {
        $this->mediaService = $mediaService;
    }

    /**
     * @return array
     */
    public static function getSubscribedEvents()
    {
        return [FormEvents::PRE_SUBMIT => 'preSubmit'];
    }

    /**
     * @param FormEvent $event
     */
    public function preSubmit(FormEvent $event)
    {
        if ($this->mediaService === null) {
            return;
        }

        $content = $event->getData();

        if ($content === null) {
            return;
        }

        $pattern = '/<img\s[^>]*?src\s*=\s*[\'\"]([^\'\"]*?)\?mediaId=([0-9]*)[^\'\"]*[\'\"][^>]*?>/';
        preg_match_all($pattern, $content, $matches);

        foreach ($matches[0] as $index => $imgTag) {
            $mediaId = intval($matches[2][$index]);
            $oldUrl = $matches[1][$index];

            $pattern = '/style\s*=\s*[\'\"]([^\'\"]*?)[\'\"]/';
            preg_match($pattern, $imgTag, $styleMatches);

            if (sizeof($styleMatches) != 2) {
                continue;
            }

            $media = $this->mediaService->find($mediaId);
            $image = $this->mediaService->createInstance($media);
            $newImgTag = $imgTag;

            list($width, $newImgTag) = $this->getSize('width', $styleMatches[1], $newImgTag);
            list($height, $newImgTag) = $this->getSize('height', $styleMatches[1], $newImgTag);

            if ($width == null && $height == null) {
                continue;
            }

            $image->cropResize($width, $height);

            $newUrl = $image->__toString();
            $newImgTag = str_replace($oldUrl, $newUrl, $newImgTag);

            $content = str_replace($imgTag, $newImgTag, $content);
        }
        $event->setData($content);
    }

    /**
     * @param string $type
     * @param string $style
     * @param string $newImgTag
     * @return array
     */
    private function getSize($type, $style, $newImgTag)
    {
        $widthPattern = sprintf('/%s:\s*(\d+|\d*\.\d+)(px);/', $type);
        preg_match($widthPattern, $style, $sizeMatches);

        if (sizeof($sizeMatches) != 3) {
            return [null, $newImgTag];
        }

        $size = doubleval($sizeMatches[1]);
        $newImgTag = str_replace($sizeMatches[0], '', $newImgTag);

        return [$size, $newImgTag];
    }
}
