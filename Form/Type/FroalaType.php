<?php
/*
 * This file is part of cwd froala bundle.
 *
 * (c)2015 Ludwig Ruderstaller <lr@cwd.at>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace Cwd\FroalaBundle\Form\Type;

use Cwd\FroalaBundle\Form\EventListener\ResizeUploadedImagesSubscriber;
use Symfony\Component\Form\AbstractTypeExtension;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class FroalaType
 * @package Cwd\FroalaBundle\Form\Type
 */
class FroalaType extends TextareaType
{
    /**
     * @var ResizeUploadedImagesSubscriber
     */
    protected $subscriber;

    /**
     * FroalaImageType constructor.
     * @param ResizeUploadedImagesSubscriber $subscriber
     */
    public function __construct(ResizeUploadedImagesSubscriber $subscriber)
    {
        $this->subscriber = $subscriber;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        $builder->addEventSubscriber($this->subscriber);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefault('attr', ['class' => 'cwd-froala']);
    }
}
