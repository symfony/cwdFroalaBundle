<?php
/*
 * This file is part of CwdFroalaBundle
 *
 * (c)2016 Ludwig Ruderstaller <lr@cwd.at>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/
namespace Cwd\FroalaBundle\Controller;

use Cwd\MediaBundle\Service\MediaService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class ImageController
 *
 * @package Cwd\FroalaBundle\Controller
 * @author  Ludwig Ruderstaller <lr@cwd.at>
 *
 * @Route("/image")
 */
class ImageController extends Controller
{
    /**
     * @Route("/upload")
     *
     * @param Request $request
     * @return mixed
     * @throws \Cwd\MediaBundle\MediaException
     */
    public function uploadAction(Request $request)
    {
        $service = $this->get('cwd.media.service');

        $media = $service->create($request->files->get('file'), true);
        $service->flush();

        $data = ['link' => $this->getUrl($media, 1000)];

        return JsonResponse::create($data);
    }

    /**
     * @Route("/list/")
     * @param Request $request
     * @return JsonResponse
     */
    public function listAction(Request $request)
    {
        $service = $this->get('cwd.media.service');
        $medias = $service->findAllImages();
        $images = [];

        foreach ($medias as $media) {
            $images[] = [
                'url'   => $this->getUrl($media, 1000),
                'thumb' => $this->getUrl($media, 150, 150, true),
            ];
        }

        return new JsonResponse($images);
    }

    /**
     * @param $media
     * @param null $width
     * @param null $height
     * @return string
     */
    private function getUrl($media, $width = null, $height = null, $crop = false)
    {
        $service = $this->get('cwd.media.service');
        $image = $service->createInstance($media);
        if ($crop) {
            $image = $image->zoomCrop($width, $height);
        } else {
            $image = $image->cropResize($width, $height);
        }

        $url = sprintf(
            "%s?mediaId=%d",
            $image->__toString(),
            $media->getId()
        );

        return $url;
    }
}
